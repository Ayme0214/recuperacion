package facci.evelynvaldez.recuperacion;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

import com.bumptech.glide.provider.ModelToResourceClassCache;

import java.util.List;

import facci.evelynvaldez.recuperacion.Adapter.AdapterTvmaze;
import facci.evelynvaldez.recuperacion.Adapter.TvMazeAdapter;
import facci.evelynvaldez.recuperacion.models.ModelTvMaze;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    TvMazeAdapter tvMazeAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //list_person en id de layout
        listView = findViewById(R.id.list_person);
        listUser();
    }

    private void listUser() {
        AdapterTvmaze adapterTvmaze = new AdapterTvmaze();
        Call<List<ModelTvMaze>> call = adapterTvmaze.getData();
        call.enqueue(new Callback<List<ModelTvMaze>>() {
            @Override
            public void onResponse(Call<List<ModelTvMaze>> call, Response<List<ModelTvMaze>> response) {
                List<ModelTvMaze> list = response.body();
                for (ModelTvMaze modelTvMaze : list){
                    modelTvMaze.getId();
                }
                tvMazeAdapter = new TvMazeAdapter(MainActivity.this, list);
                listView.setAdapter(tvMazeAdapter);
            }

            @Override
            public void onFailure(Call<List<ModelTvMaze>> call, Throwable t) {

            }
        });
    }
}