package facci.evelynvaldez.recuperacion.models;

import com.google.gson.annotations.SerializedName;

public class ModelTvMaze {
    @SerializedName("id")
    String id;
    @SerializedName("url")
    String url;
    @SerializedName("name")
    String name;

    public ModelTvMaze() {
    }


    public ModelTvMaze(String id, String url, String name) {
        this.id = id;
        this.url = url;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
