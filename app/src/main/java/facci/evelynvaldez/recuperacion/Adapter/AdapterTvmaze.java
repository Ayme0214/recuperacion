package facci.evelynvaldez.recuperacion.Adapter;

import java.util.List;

import facci.evelynvaldez.recuperacion.constants.Api;
import facci.evelynvaldez.recuperacion.models.ModelTvMaze;
import facci.evelynvaldez.recuperacion.services.ServiceTvMaze;
import retrofit2.Call;

public class AdapterTvmaze extends BaseAdapter implements ServiceTvMaze {

    private ServiceTvMaze serviceTvMaze;

    public AdapterTvmaze(){
        super(Api.BASE_URL);
        serviceTvMaze = createService(ServiceTvMaze.class);
    }

    @Override
    public Call<List<ModelTvMaze>> getData() {
        return serviceTvMaze.getData();
    }
}
