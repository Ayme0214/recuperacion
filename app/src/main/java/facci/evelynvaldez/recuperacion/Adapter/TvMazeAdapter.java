package facci.evelynvaldez.recuperacion.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import facci.evelynvaldez.recuperacion.ActivityView;
import facci.evelynvaldez.recuperacion.models.ModelTvMaze;
import facci.evelynvaldez.recuperacion.R;

public class TvMazeAdapter extends BaseAdapter {
        private Context context;
        private List<ModelTvMaze> list;

        public TvMazeAdapter(Context context, List<ModelTvMaze> list) {
            this.context = context;
            this.list = list;
        }

        @Override
        public int getCount() {return list.size();}

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            if(convertView == null){
                convertView = View.inflate(context,  R.layout.list_person, null);
            }

            TextView id = convertView.findViewById(R.id.txtId);
            TextView url = convertView.findViewById(R.id.txtUrl);
            TextView name = convertView.findViewById(R.id.txtName);
            //ImageView img = convertView.findViewById(R.id.image);
            Button btnView = convertView.findViewById(R.id.btnView);

            ModelTvMaze modelTvMaze = list.get(position);

            //Glide.with(context).load(modelTest.getImg()).into(img);
            id.setText(modelTvMaze.getId());
            url.setText(modelTvMaze.getUrl());
            name.setText(modelTvMaze.getName());
            btnView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    context.startActivity(new Intent(context, ActivityView.class).putExtra("id", modelTvMaze.getId()));
                    context.startActivity(new Intent(context, ActivityView.class).putExtra("url", modelTvMaze.getUrl()));
                    context.startActivity(new Intent(context, ActivityView.class).putExtra("name", modelTvMaze.getName()));
                }
            });

            return convertView;
        }


    }

