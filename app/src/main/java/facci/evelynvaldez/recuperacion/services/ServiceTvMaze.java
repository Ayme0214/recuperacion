package facci.evelynvaldez.recuperacion.services;

import java.util.List;

import facci.evelynvaldez.recuperacion.constants.Api;
import facci.evelynvaldez.recuperacion.models.ModelTvMaze;
import retrofit2.Call;
import retrofit2.http.GET;

public interface ServiceTvMaze {


    //se crea la peticion en base a url get
    //LIST
    @GET(Api.URL_GET)
    Call<List<ModelTvMaze>> getData();

}
